require 'redmine'

require 'redmine_jbox_support'

VERSION_NUMBER = '1.0.0'

Redmine::Plugin.register :redmine_jbox_support do
  name 'Redmine JBox Support'
  author 'Nicolas Rodriguez'
  description 'This is a plugin for Redmine to manage Customer Support'
  version VERSION_NUMBER
  url 'http://www.jbox-web.com'
  author_url 'mailto:nrodriguez@jbox-web.com'

  requires_redmine :version_or_higher => '1.4.5'

  project_module :jbox_support_module do
    permission :edit_status,          :issues => :index
    permission :edit_priority,        :issues => :index
    permission :edit_start_date,      :issues => :index
    permission :edit_due_date,        :issues => :index
    permission :edit_estimated_hours, :issues => :index
    permission :edit_done_ratio,      :issues => :index
    permission :edit_fixed_version,   :issues => :index
    permission :be_a_watcher,         :issues => :index
    permission :see_project_members,  :issues => :index
    permission :edit_my_page,         :issues => :index

    # also used in Redmine Contacts Plugin
    permission :edit_assigned_to,     :issues => :index
    permission :watch_contacts,       :contacts => :index
    permission :see_related_projects, :contacts => :index
  end

  settings :partial => 'settings/jbox_support_settings'
end

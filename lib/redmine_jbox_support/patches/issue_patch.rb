module RedmineJboxSupport
  module Patches
    module IssuePatch
      unloadable

      def self.included(base)
        base.send(:include, InstanceMethods)
        base.class_eval do
          validates_format_of :author_mail, :with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i, :allow_blank => false
          validates_presence_of :author_name
          validate :validate_things
        end
      end

      module InstanceMethods

        def validate_things
          role = User.current.roles_for_project(@project)
          if role[0]['name'] == JboxSupportSettings[:role_name]
            if author_mail == User.current.mail
              errors.add :author_mail, :invalid_author_mail_reserved
              false
            else
              author.firstname, author.mail = author_name, author_mail
              author
            end
          else
            author.firstname, author.mail = author_name, author_mail
            author
          end
        end

      end

    end
  end
end

Dispatcher.to_prepare do
  unless Issue.included_modules.include?(RedmineJboxSupport::Patches::IssuePatch)
    Issue.send :include, RedmineJboxSupport::Patches::IssuePatch
  end
end

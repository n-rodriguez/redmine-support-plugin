require_dependency 'issue'

module RedmineJboxSupport
  module Patches
    module IssuesControllerPatch
      unloadable

      def self.included(base)
        base.send(:include, InstanceMethods)
        base.class_eval do
          before_filter :recognize_support, :only => [:create, :update]
          after_filter  :create_issue_contact, :only => :create
        end
      end

      module InstanceMethods

        def recognize_support
          if params[:action] == 'create' && params[:issue]
            if params[:author_name] && params[:author_mail]
              @issue.author_name = params[:author_name]
              @issue.author_mail = params[:author_mail]
              true
            else
              flash[:error] = "Un des champs hidden (author_name ou author_mail) est vide..."
              false
            end
          end
        end

        def create_issue_contact
          if @issue.errors.empty?
            puts "JDEBUG : good"
            role = User.current.roles_for_project(@project)
            if role[0]['name'] == JboxSupportSettings[:role_name]
              my_contact = Contact.find_by_email(params[:author_mail])
              if !my_contact
                puts "JDEBUG : contact does not exist"
                contact = Contact.new
                contact.first_name = params[:author_name]
                contact.email = params[:author_mail]
                contact.projects << @project
                contact.issues << @issue
                contact.save
                true
              else
                puts "JDEBUG : contact already exists, just linking to the new issue and to the project if not already done"
                if not my_contact.projects.include?(@project)
                  my_contact.projects << @project
                end
                my_contact.issues << @issue
                my_contact.save
                true
              end
            else
              puts "JDEBUG : do nothing"
              true
            end
          else
            puts "JDEBUG : errors are printed in the interface"
            #~ puts YAML::dump(@issue.errors)
            true
          end
        end

      end

    end
  end
end

Dispatcher.to_prepare do
  unless IssuesController.included_modules.include?(RedmineJboxSupport::Patches::IssuesControllerPatch)
    IssuesController.send :include, RedmineJboxSupport::Patches::IssuesControllerPatch
  end
end

require_dependency 'query'

module RedmineJboxSupport
  module Patches
    module QueryPatch
      unloadable

      def self.included(base)
        base.extend(ClassMethods)
        base.send(:include, InstanceMethods)
        base.class_eval do
          base.add_available_column(QueryColumn.new(:author_name, :sortable => "#{Issue.table_name}.author_name"))
          base.add_available_column(QueryColumn.new(:author_mail, :sortable => "#{Issue.table_name}.author_mail"))
          alias_method_chain :available_filters, :jbox_support
        end
      end

      module ClassMethods

        # Setter for +available_columns+ that isn't provided by the core.
        def available_columns=(v)
          self.available_columns = (v)
        end

        # Method to add a column to the +available_columns+ that isn't provided by the core.
        def add_available_column(column)
          self.available_columns << (column)
        end
      end

      module InstanceMethods

        def available_filters_with_jbox_support
          return @available_filters if @available_filters

          available_filters_without_jbox_support

          @available_filters["author_name"] = { :type => :text, :order => 5, :name => l(:field_author_name) }
          @available_filters["author_mail"] = { :type => :text, :order => 5, :name => l(:field_author_mail) }
          @available_filters
        end

        def sql_for_author_name_field(field, operator, v)
          "(#{sql_for_field(field, operator, v, Issue.table_name, field)} OR #{Issue.table_name}.author_id IN " +
              "(SELECT id FROM #{User.table_name} WHERE #{sql_for_field(field, operator, v, User.table_name, "firstname")} " +
              "OR #{sql_for_field(field, operator, v, User.table_name, "lastname")}))"
        end

        def sql_for_author_mail_field(field, operator, v)
          "(#{sql_for_field(field, operator, v, Issue.table_name, field)} OR #{Issue.table_name}.author_id IN " +
              "(SELECT id FROM #{User.table_name} WHERE #{sql_for_field(field, operator, v, User.table_name, "mail")}))"
        end

      end

    end
  end
end

Dispatcher.to_prepare do
  unless Query.included_modules.include?(RedmineJboxSupport::Patches::QueryPatch)
    Query.send :include, RedmineJboxSupport::Patches::QueryPatch
  end
end

module RedmineJboxSupport
  module Patches
    module MailHandlerPatch
      unloadable

      def self.included(base)
        base.send(:include, InstanceMethods)
        base.class_eval do
          alias_method_chain :dispatch, :anonymous
        end
      end

      module InstanceMethods

        def dispatch_with_anonymous
          if user.anonymous?
            user.mail, user.name = from_address, from_name if from_address
          end
          dispatch_without_anonymous
        end

        def from_object
          if Redmine::VERSION::MAJOR >= 2
            email['from'].decoded
            email['from'].addrs.first
          else
            email.from_addrs.first
          end
        end

        def from_address
          from_object && from_object.address
        end

        def from_name
          if from_object
            if Redmine::VERSION::MAJOR >= 2
              from_object.name
            else
              TMail::Unquoter.unquote_and_convert_to(from_object.name, 'utf-8')
            end
          end
        end

      end

    end
  end
end

Dispatcher.to_prepare do
  unless MailHandler.included_modules.include?(RedmineJboxSupport::Patches::MailHandlerPatch)
    MailHandler.send :include, RedmineJboxSupport::Patches::MailHandlerPatch
  end
end

require 'dispatcher'

require 'redmine_jbox_support/patches/issue_patch'
require 'redmine_jbox_support/patches/issues_controller_patch'
require 'redmine_jbox_support/patches/mail_handler_patch'
require 'redmine_jbox_support/patches/query_patch'

class JboxSupportSettings
  # Returns the value of the setting named name
  def self.[](name)
    RedmineJboxSupport.settings[name]
  end

end

module RedmineJboxSupport
  def self.settings()
    Setting[:plugin_redmine_jbox_support]
  end
end
